var myName = "Aleya";
console.log(myName); // printing in console

//Javascript is case sensetive

var planet = 123;
var PLANET = 12;
console.log(planet,PLANET); // These two are different.

//Naming of variable should be meaningfull
//Variable name should be start with letter
//Semicolon after code is a good practice in programming but without semicolon it will work
//Reserved keyword like var let can not be used in Javascript
var myNumber = 12;
myNumber     = 13;
console.log(myNumber);// console log is used to print in javascript

myNumber     = 14;

console.log(myNumber);

var myName   =  "Dracula";
var myAge    =  100000;
console.log("My name is ",myName,"and my age is",myAge,"years");


const pi     =  3.1416; // const means constant the variable value will be set once and it will never change
// pi           =  444;
console.log("Value of pi is ",pi);

