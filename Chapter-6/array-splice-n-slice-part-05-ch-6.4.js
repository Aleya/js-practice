//Array Slice
var list = [
  "sun",//0 = -7
  "mon",//1 = -6
  "tue",//2 = -5
  "wed",//3 = -4
  "thu",//4 = -3
  "fri",//5 = -2
  "sat" //6 = -1
];

var chunk         = list.slice(1,4);
var sliceAll      = list.slice(1);//1 theke last porjonto ber kore niye ashbe

var chunkLast3    = list.slice(-3);
var chunkLast3to5 = list.slice(-5,-3);


console.log(list,chunk);


//Array Splice
var list = [
    "sun",//0 = -7
    "mon",//1 = -6
    "tue",//2 = -5
    "wed",//3 = -4
    "thu",//4 = -3
    "fri",//5 = -2
    "sat" //6 = -1
];

var length = list.length;

for (var i=0;i<length;i++){
    console.log("Element at ",i," offset is",list[i]);
}

//for reverse
for (var i=list.length-1;i>=0;i--){
    console.log("Element at ",i," offset is",list[i]);
}

for (i in list){
    console.log("Element at ",i," offset is",list[i]);
}
