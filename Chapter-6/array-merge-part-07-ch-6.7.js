var list1 = [
    "sun",
    "mon",
    "tue",
];

var list2 = [
    "wed",
    "thu",
];

var list3 = [
    "fri",
    "sat",
];


var list = list1.concat(list2,list3);

//or

var list = [].concat(list1,list2,list3);

console.log(list);