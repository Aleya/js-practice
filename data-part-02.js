var a  = 12;
var b  = 13;
var c  = 14;
var d  = 15;

// variable declaration in one line
var a = 12, b = 13, c = 14;

console.log(a,b,c);


/*There are three types of variables
1. string
2. number
3. boolean
4. undefined
5. null
*/

var word         =  "earth";
var pi           =  3.1416;
var eartIsRound  =  true;

console.log(typeof word,typeof pi,typeof eartIsRound);

var aleya;// undefined variable
var nullCheck    = null; //special type variable and the variable will be empty

console.log(nullCheck); //special type object
