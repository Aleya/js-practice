var monthName   = "April, 2019";
var days        = 30;
var startingDay = 3;

console.log("\n\n Calender 01 ",monthName,"\n");
console.log("sun  mon  tue  wed  thu  fri  sat");


for (var i=0; i<5; i++){
    var dayRow = "";
    for (var j=1;j<=7;j++){
        var currentDay = 7*i + j - startingDay;
        if (currentDay == days){
            break;
        }else if(currentDay<1){
            currentDay = " ";
        }

        if (currentDay>9){
            dayRow += currentDay + "   ";

        }else {
            dayRow += currentDay + "    ";
        }

    }
    console.log(dayRow);
}