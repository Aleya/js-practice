
// for (initialization;condition;stepping){
//
// }


//infinity loop
// var i = 0;
// for (;;){
//     i++;
//     console.log(i);
// }

var i = 0;

for (i=0;i<10;i+=2){
    console.log(i);
}


var i = 0;

for (;;){
    i++;
    console.log(i);
    if (10 == i){
        break;
    }
}
