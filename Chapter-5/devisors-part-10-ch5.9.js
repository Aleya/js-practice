
// Math.ceil() = 11.6 = 12;
// Math.floor() = 11.6 = 12;
var n = 144;
var range  =  Math.ceil(Math.sqrt(n));

var devisors = "";

for (var i=1; i<=range;i++){
    if (n%i == 0){
        if (i == n/i){
            devisors += i + " ";
        }
        devisors += i + " " + (n/i) + " ";
    }
}
console.log(devisors);