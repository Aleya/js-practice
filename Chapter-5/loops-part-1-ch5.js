var i = 10;

while (i-- > 0){
    console.log(i);
}

//infinity loop
// var i = 0;
//
// while (true){
//     i++;
//     console.log(i);
// }

//(ctrl+C) loop theke ber howar upay


var i = 0;

while (i<= 10){
    console.log(i);
    i += 2;
}

var i = 0;
while (true){
    console.log(i);
    if (10 == i){
        break;
    }
    i++;
}