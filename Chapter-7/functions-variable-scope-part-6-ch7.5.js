var i = 12;

function doSomething() {
    var i = 10;//function er vitore redeclare korle globally value change hobena

    console.log("Inside Function", i);


}

doSomething();
console.log("Outside Function",i);

//hoisting(jekhanei declare kora hok na keno sheta upore niye chole ashe)
function doSomething() {
    i = 10;//function er vitore redeclare korle globally value change hobena

    console.log("Inside Function", i);
    var i;

}

function doSomething() {
    var  i = 10;
    if (true){
     var i = 13;
    }
    console.log("Inside Function", i);
}

let i = 19;

function doSomething() {
    let i = 20;//function er vitore redeclare korle globally value change hobena

    console.log("Inside Function", i);


}

doSomething();
console.log("Outside Function",i);

let i = 15;

function doSomething() {
    let  i = 16;
    if (true){
        let i = 17;
        console.log("Inside if", i);
    }
    console.log("Inside Function", i);


}

doSomething();
console.log("Outside Function",i);