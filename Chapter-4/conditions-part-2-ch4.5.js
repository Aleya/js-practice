var personOne       = "jamal";
var personTwo       = "jamal";
var areTheyBrother  = true;


//Nested Loop
if ("jamal" == personOne){
    if ("kamal" == personTwo){
        if (areTheyBrother){
            console.log("Jamal And Kamal are brothers!");
        } else {
            console.log("Jamal And Kamal are not brothers!");
        }
    }
}

//And Operator
if ("jamal" == personOne && "kamal" == personTwo && areTheyBrother) {
    console.log("Jamal And Kamal are brothers!");
} else {
    console.log("Jamal And Kamal are not brothers!");
}

//And Operator
var n = 7;

if(n<10){
    if (2 == n || 3 == n || 5 == n || 7 == n){
        console.log("This number",n,"is smaller then 10 and it's a prime number");
    }else {
        console.log("This number",n,"is smaller then 10 and it's Not a prime number");
    }
}else {
    console.log("This number",n,"is greater than 10");
}