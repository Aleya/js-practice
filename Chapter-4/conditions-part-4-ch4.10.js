var n =  12;
var result;

var remainder =  n%2;

//ternary operator
result = (0 == remainder)? "even":"odd";

console.log("This number is ",result);

result = (n < 0)? "negative":"positive";

console.log("This number is ",result);